#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools

def setup():
    # suppress compiler warnings
    pisitools.cflags.add("-Wno-unused-result \
                         -Wno-deprecated-declarations \
                         -Wno-implicit-function-declaration")
    pisitools.cxxflags.add("-Wno-deprecated-declarations")
    
    autotools.configure("--with-dbus \
                         --disable-static \
                         --disable-gtk-doc \
                         --with-authfw=pam \
                         --disable-examples \
                         --disable-man-pages \
                         --enable-introspection \
                         --with-mozjs=mozjs-17.0 \
                         --enable-libelogind=yes \
                         --with-os-type=Pisilinux \
                         --enable-libsystemd-login=no \
                         --libexecdir=/usr/lib/polkit-1 \
                         --with-systemdsystemunitdir=no \
                         --with-pam-module-dir=/lib/security/")
    
    # fix unused direct dependency analysis
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ") 

def build():
    shelltools.export('HOME', get.workDIR())
    autotools.make()

def install():
    autotools.rawInstall("DESTDIR=%s/" % get.installDIR())

    pisitools.dodir("/var/lib/polkit-1")
    shelltools.chmod("%s/var/lib/polkit-1" % get.installDIR(), mode=00700)
    shelltools.chmod("%s/etc/polkit-1/rules.d" % get.installDIR(), mode=00700)
    shelltools.chown("%s/etc/polkit-1/rules.d" % get.installDIR(),"polkitd","root") #yada? "polkitd","root"
    shelltools.chown("%s/var/lib/polkit-1" % get.installDIR(),"polkitd","polkitd")  
    shelltools.chown("%s/usr/share/polkit-1" % get.installDIR(),"polkitd","root") #yada? "polkitd","root"
    pisitools.dodoc("README", "HACKING", "COPYING")