# _*_ coding: utf-8 _*_
from comar.service import *

serviceType = "local"
serviceDesc = _({"en": "System Message Logger",
                 "tr": "Sistem Günlükleme Hizmeti"})
serviceDefault = "on"

PIDFILE = "/var/run/syslogd.pid"

@synchronized
def start():
    startService(command="/usr/bin/syslog-ng",
                 args=config.get("SYSLOGD_OPTIONS", ""),
                 pidfile=PIDFILE,
                 detach=True)

@synchronized
def stop():
    stopService(pidfile=PIDFILE,
                donotify=True)

def status():
    return isServiceRunning(pidfile=PIDFILE)
