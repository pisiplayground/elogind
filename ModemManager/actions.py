#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import autotools
from pisi.actionsapi import shelltools

def setup():
    shelltools.export("LIBSYSTEMD_LOGIN_CFLAGS", "libelogind")
    shelltools.export("LIBSYSTEMD_LOGIN_LIBS", "libelogind")
    shelltools.system("NOCONFIGURE=1 ./autogen.sh")
    autotools.configure("--disable-static \
                         --enable-gtk-doc \
                         --with-polkit=permissive \
                         --enable-more-warnings=no \
                         --with-systemd-journal=no \
                         --enable-more-warnings=yes \
                         --with-udev-base-dir=/lib/udev \
                         --with-systemd-suspend-resume=yes")
    
    pisitools.dosed("libtool", " -shared ", " -Wl,-O1,--as-needed -shared ")

def build():
    autotools.make()
"""
def check():
    autotools.make("-k check")
"""

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

    pisitools.dodoc("README", "COPYING")