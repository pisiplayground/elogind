#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import get
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import cmaketools

def setup():
    shelltools.makedirs("build")
    shelltools.cd("build")    

    cmaketools.configure("-DENABLE_PAM=yes \
                          -DNO_SYSTEMD=yes \
                          -DUSE_WAYLAND=ON \
                          -DUSE_ELOGIND=yes \
                          -DBUILD_MAN_PAGES=ON \
                          -DCMAKE_BUILD_TYPE=Release \
                          -DCMAKE_INSTALL_PREFIX=/usr \
                          -DCMAKE_INSTALL_LIBEXECDIR=/usr/lib/sddm \
                          -DDBUS_CONFIG_FILENAME=sddm_org.freedesktop.DisplayManager.conf", sourceDir=".." )

def build():
    shelltools.makedirs("build")
    shelltools.cd("build")

    cmaketools.make()

def install():
    shelltools.makedirs("build")
    shelltools.cd("build")

    cmaketools.install()
    
    pisitools.insinto("/usr/share/sddm/themes/pisi-linux-gray", "../pisi-linux-gray-0.1/*")

    pisitools.dodoc("../LICENSE")
    ## If you don't like to see any character at all not even while being entered set this to true.
    pisitools.dosed("%s/usr/share/sddm/themes/pisi-linux-gray/theme.conf" % get.installDIR(), 'ForceHideCompletePassword="false"', 'ForceHideCompletePassword="true"')